/*Michael Sargent, Jacob Chadwick, Kyle Welch
Lab 2 - SCC Database Creation, Data and Queries
1/23/18
*/

DROP DATABASE IF EXISTS scc;

CREATE DATABASE scc;

USE scc;

CREATE TABLE student
(
	s_id INT	    NOT NULL,
	s_fname VARCHAR(20) NOT NULL,
	s_lname VARCHAR(40) NOT NULL,
	address VARCHAR(40) NOT NULL DEFAULT 'UNKNOWN',
	city VARCHAR(20)  NULL,
	state CHAR(2)	  NULL,
	zip CHAR(5)		  NULL,
	gender CHAR(1)	  NOT NULL,
	
	PRIMARY KEY(s_id)
);

CREATE TABLE teachers
(
	t_id int NOT NULL,
	t_fname varchar(20) NOT NULL,
	t_lname varchar(40) NOT NULL,
	phone char(12)	NOT NULL DEFAULT 'UNKNOWN',
	salary DOUBLE NOT NULL DEFAULT 0,
	PRIMARY KEY(t_id),
	CHECK (phone like '[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]')
);

CREATE TABLE dept
(
	dept_id CHAR(4) NOT NULL,
	descr VARCHAR(20) NOT NULL,
	chair INT NOT NULL,
	PRIMARY KEY(dept_id),
	FOREIGN KEY(chair) REFERENCES teachers(t_id) 
);

CREATE TABLE courses
(
	c_id CHAR(7) NOT NULL,
	descr VARCHAR(40) NOT NULL,
	dept_id CHAR(4) NOT NULL,
	credits INT NOT NULL,
	PRIMARY KEY(c_id),
	FOREIGN KEY(dept_id) REFERENCES dept(dept_id)
);

CREATE TABLE enroll_info
(
	s_id INT NOT NULL,
	c_id CHAR(7) NOT NULL,
	status VARCHAR(2) NOT NULL,
	term VARCHAR(7) NOT NULL,
	grade INT NOT NULL,
	PRIMARY KEY(s_id, c_id),
	FOREIGN KEY(c_id) REFERENCES courses(c_id),
	FOREIGN KEY(s_id) REFERENCES student(s_id)
);

CREATE TABLE sections
(
	c_id char(7) NOT NULL,
	sec_id varchar(6) NOT NULL,
	t_id int NOT NULL,
	sec_day varchar(3) NOT NULL,
	sec_time char(11) NOT NULL,
	term varchar(7) NOT NULL,
	PRIMARY KEY(c_id, sec_id),
	FOREIGN KEY(c_id) REFERENCES courses(c_id),
	CHECK (sec_time like '[0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9]')
);

CREATE TABLE student_courses
(
	s_id INT NOT NULL,
	c_id CHAR(7) NOT NULL,
	sec_id  VARCHAR(6) NOT NULL,
	PRIMARY KEY(s_id, c_id, sec_id)
);


CREATE TABLE requirements
(
	c_id char(7) NOT NULL,
	c_requirement char(7) NOT NULL,
	PRIMARY KEY(c_id, c_requirement),
	FOREIGN KEY(c_id) REFERENCES courses(c_id)
);

INSERT student VALUES(1, 'Michael', 'Sargent', '4 Morgan Dr', 'Bow', 'NH', '03304', 'M');
INSERT student VALUES(2, 'Jake', 'Chadwick', '123 Main St', 'Concord', 'NH', '03301', 'M');
INSERT student VALUES(3, 'Samantha', 'White', '145 Elm St', 'Manchester', 'NH', '03104', 'F');
INSERT student VALUES(4, 'Laura', 'Jobin', '87 Pine St', 'Hooksett', 'NH', '03106', 'F');

INSERT teachers VALUES(1, 'Kenneth', 'Polito', '603-123-4567', 80000.02);
INSERT teachers VALUES(2, 'Frank', 'Gitlitz', '603-987-6543', 80000.04);

INSERT dept VALUES('ENGL', 'English', 1);
INSERT dept VALUES('HIST', 'History', 1);
INSERT dept VALUES('MATH', 'Math', 2);
INSERT dept VALUES('COMP', 'Computer Science', 2);

INSERT courses VALUES('ENGL101', 'Intro To Spanish', 'ENGL', 4);
INSERT courses VALUES('COMP282', 'Turning On A Computer', 'COMP', 3);
INSERT courses VALUES('MATH123', '1 + 2 = Fish', 'MATH', 7);
INSERT courses VALUES('HIST969', 'Colonizing The Moon', 'HIST', 2);

INSERT sections VALUES('COMP282', 'Day', 1, 'MWF', '08:00-08:50', 'FAL2016');
INSERT sections VALUES('COMP282', 'Night', 1, 'TR', '17:00-18:20', 'FAL2016');
INSERT sections VALUES('MATH123', 'Online', 2, 'MWF', '12:00-12:50', 'SPR2017');
INSERT sections VALUES('ENGL101', 'Day', 1, 'TR', '02:00-03:20', 'SUM2017');
INSERT sections VALUES('HIST969', 'Online', 2, 'TR', '15:00-16:20', 'SUM2017');

INSERT enroll_info VALUES(1, 'COMP282', 'P', 'FAL2016', 97);
INSERT enroll_info VALUES(1, 'ENGL101', 'EN', 'SUM2017', 97);
INSERT enroll_info VALUES(1, 'MATH123', 'W', 'SPR2017', 74);
INSERT enroll_info VALUES(1, 'HIST969', 'EN', 'SUM2017', 86);
INSERT enroll_info VALUES(2, 'MATH123', 'P', 'SPR2017', 92);
INSERT enroll_info VALUES(2, 'HIST969', 'EN', 'SUM2017', 92);
INSERT enroll_info VALUES(3, 'ENGL101', 'EN', 'SUM2017', 91);
INSERT enroll_info VALUES(3, 'COMP282', 'F', 'FAL2016', 54);
INSERT enroll_info VALUES(4, 'COMP282', 'P', 'FAL2016', 91);
INSERT enroll_info VALUES(4, 'HIST969', 'EN', 'HIST969', 88);

INSERT student_courses VALUES(1, 'COMP282', 'Day');
INSERT student_courses VALUES(1, 'ENGL101', 'Day');
INSERT student_courses VALUES(1, 'MATH123', 'Online');
INSERT student_courses VALUES(1, 'HIST969', 'Online');
INSERT student_courses VALUES(2, 'MATH123', 'Day');
INSERT student_courses VALUES(2, 'HIST969', 'Online');
INSERT student_courses VALUES(3, 'ENGL101', 'Day');
INSERT student_courses VALUES(3, 'COMP282', 'Night');
INSERT student_courses VALUES(4, 'COMP282', 'Day');
INSERT student_courses VALUES(4, 'HIST969', 'Online');

/* Query 1: All Fall 2016 courses and average grades */
MariaDB [scc]> SELECT courses.c_id, term, AVG(grade) FROM courses JOIN enroll_info ON enroll_info.c_id = courses.c_id WHERE term = 'FAL2016';
+---------+---------+------------+
| c_id    | term    | AVG(grade) |
+---------+---------+------------+
| COMP282 | FAL2016 |    80.6667 |
+---------+---------+------------+
1 row in set (0.03 sec)

/* Query 2: Courses a student is currently enrolled */
MariaDB [scc]> SELECT * FROM enroll_info WHERE status = 'EN' AND s_id = 1;
+------+---------+--------+---------+-------+
| s_id | c_id    | status | term    | grade |
+------+---------+--------+---------+-------+
|    1 | ENGL101 | EN     | SUM2017 |    97 |
|    1 | HIST969 | EN     | SUM2017 |    86 |
+------+---------+--------+---------+-------+
2 rows in set (0.00 sec)

/* Query 3: Courses taught by a teacher for one term */
MariaDB [scc]> SELECT sections.c_id, sec_id, t_id, sec_day,term, credits FROM sections JOIN courses ON courses.c_id = sections.c_id WHERE t_id = 1 AND term = 'FAL2016';
+---------+--------+------+---------+---------+---------+
| c_id    | sec_id | t_id | sec_day | term    | credits |
+---------+--------+------+---------+---------+---------+
| COMP282 | Day    |    1 | MWF     | FAL2016 |       3 |
| COMP282 | Night  |    1 | TR      | FAL2016 |       3 |
+---------+--------+------+---------+---------+---------+
2 rows in set (0.00 sec)

/* Query 4: Transcript for a student */
MariaDB [scc]> SELECT enroll_info.c_id, credits,  term, grade FROM enroll_info JOIN courses ON enroll_info.c_id = courses.c_id WHERE s_id = 1;
+---------+---------+---------+-------+
| c_id    | credits | term    | grade |
+---------+---------+---------+-------+
| COMP282 |       3 | FAL2016 |    97 |
| ENGL101 |       4 | SUM2017 |    97 |
| HIST969 |       2 | SUM2017 |    86 |
| MATH123 |       7 | SPR2017 |    74 |
+---------+---------+---------+-------+
4 rows in set (0.00 sec)

/* To get student's schedule */
MariaDB [scc]> SELECT enroll_info.s_id, enroll_info.c_id, sec_time FROM enroll_info JOIN student_courses ON enroll_info.c_id = student_courses.c_id JOIN sections ON sections.sec_id = student_courses.sec_id WHERE enroll_info.s_id = student_courses.s_id AND student_courses.s_id = 1 AND student_courses.c_id = sections.c_id;
+------+---------+-------------+
| s_id | c_id    | sec_time    |
+------+---------+-------------+
|    1 | COMP282 | 08:00-08:50 |
|    1 | ENGL101 | 02:00-03:20 |
|    1 | HIST969 | 15:00-16:20 |
|    1 | MATH123 | 12:00-12:50 |
+------+---------+-------------+
4 rows in set (0.00 sec)
