﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidtermProject
{
    public partial class UpdateBook : Form
    {
        Book book;
        public UpdateBook(Book b)
        {
            InitializeComponent();

            book = b;

            titleIDBox.Text = book.title_id;
            titleBox.Text = book.title;
            typeBox.Text = book.type;
            pubIDComboBox.Text = book.pub_id;
            priceBox.Text = book.price;
            advanceBox.Text = book.advance;
            royaltyBox.Text = book.royalty;
            ytdBox.Text = book.ytd_sales;
            notesBox.Text = book.notes;
            pubDatePicker.Text = book.pubdate;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void updateBookButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon!");
        }

    }
}
