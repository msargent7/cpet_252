﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidtermProject
{
    public partial class UpdateStore : Form
    {
        Store store;

        string serverIP = Form1.serverIP;
        public UpdateStore(Store s)
        {
            InitializeComponent();

            store = s;
            storeIDBox.Text = store.stor_id;
            storeNameBox.Text = store.stor_name;
            storeAddressBox.Text = store.stor_address;
            cityBox.Text = store.city;
            stateComboBox.Text = store.state;
            zipBox.Text = store.zip;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void updateStoreButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon!");
        }
    }
}
