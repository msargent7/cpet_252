﻿namespace MidtermProject
{
    partial class UpdateStore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.updateStoreButton = new System.Windows.Forms.Button();
            this.zipBox = new System.Windows.Forms.TextBox();
            this.cityBox = new System.Windows.Forms.TextBox();
            this.storeAddressBox = new System.Windows.Forms.TextBox();
            this.storeNameBox = new System.Windows.Forms.TextBox();
            this.storeIDBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // stateComboBox
            // 
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Items.AddRange(new object[] {
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NH",
            "NJ",
            "NM",
            "NV",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY"});
            this.stateComboBox.Location = new System.Drawing.Point(263, 88);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(145, 21);
            this.stateComboBox.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(260, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Zip:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(260, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "State:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(260, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "City";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Store Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Store Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Store ID:";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(382, 204);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(113, 28);
            this.cancelButton.TabIndex = 35;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // updateStoreButton
            // 
            this.updateStoreButton.Location = new System.Drawing.Point(263, 204);
            this.updateStoreButton.Name = "updateStoreButton";
            this.updateStoreButton.Size = new System.Drawing.Size(113, 28);
            this.updateStoreButton.TabIndex = 34;
            this.updateStoreButton.Text = "Update Store";
            this.updateStoreButton.UseVisualStyleBackColor = true;
            this.updateStoreButton.Click += new System.EventHandler(this.updateStoreButton_Click);
            // 
            // zipBox
            // 
            this.zipBox.Location = new System.Drawing.Point(263, 145);
            this.zipBox.Name = "zipBox";
            this.zipBox.Size = new System.Drawing.Size(145, 20);
            this.zipBox.TabIndex = 33;
            // 
            // cityBox
            // 
            this.cityBox.Location = new System.Drawing.Point(263, 34);
            this.cityBox.Name = "cityBox";
            this.cityBox.Size = new System.Drawing.Size(145, 20);
            this.cityBox.TabIndex = 32;
            // 
            // storeAddressBox
            // 
            this.storeAddressBox.Location = new System.Drawing.Point(12, 145);
            this.storeAddressBox.Name = "storeAddressBox";
            this.storeAddressBox.Size = new System.Drawing.Size(145, 20);
            this.storeAddressBox.TabIndex = 31;
            // 
            // storeNameBox
            // 
            this.storeNameBox.Location = new System.Drawing.Point(12, 88);
            this.storeNameBox.Name = "storeNameBox";
            this.storeNameBox.Size = new System.Drawing.Size(145, 20);
            this.storeNameBox.TabIndex = 30;
            // 
            // storeIDBox
            // 
            this.storeIDBox.Location = new System.Drawing.Point(12, 34);
            this.storeIDBox.Name = "storeIDBox";
            this.storeIDBox.ReadOnly = true;
            this.storeIDBox.Size = new System.Drawing.Size(145, 20);
            this.storeIDBox.TabIndex = 29;
            // 
            // UpdateStore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 254);
            this.Controls.Add(this.stateComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.updateStoreButton);
            this.Controls.Add(this.zipBox);
            this.Controls.Add(this.cityBox);
            this.Controls.Add(this.storeAddressBox);
            this.Controls.Add(this.storeNameBox);
            this.Controls.Add(this.storeIDBox);
            this.Name = "UpdateStore";
            this.Text = "UpdateStore";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox stateComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button updateStoreButton;
        private System.Windows.Forms.TextBox zipBox;
        private System.Windows.Forms.TextBox cityBox;
        private System.Windows.Forms.TextBox storeAddressBox;
        private System.Windows.Forms.TextBox storeNameBox;
        private System.Windows.Forms.TextBox storeIDBox;
    }
}