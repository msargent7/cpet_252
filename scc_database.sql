DROP DATABASE IF EXISTS scc;

CREATE DATABASE scc;

USE scc;

CREATE TABLE student
(
	s_id INT		  NOT NULL,
	s_fname VARCHAR(20) NOT NULL,
	s_lname VARCHAR(40) NOT NULL,
	address VARCHAR(40) NOT NULL DEFAULT 'UNKNOWN',
	city VARCHAR(20)  NULL,
	state CHAR(2)	  NULL,
	zip CHAR(5)		  NULL,
	gender CHAR(1)	  NOT NULL,
	
	PRIMARY KEY(s_id)
);

CREATE TABLE teachers
(
	t_id 			int 			NOT NULL,
	t_fname			varchar(20)		NOT NULL,
	t_lname 		varchar(40) 	NOT NULL,
	phone			char(12)		NOT NULL DEFAULT 'UNKNOWN',
	salary			int				NOT NULL DEFAULT 0,
	PRIMARY KEY(t_id),
	CHECK (phone like '[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]')
);

CREATE TABLE dept
(
	dept_id CHAR(4)		NOT NULL,
	descr VARCHAR(20) NOT NULL,
	chair   INT NOT NULL,
	PRIMARY KEY(dept_id),
	FOREIGN KEY(chair) REFERENCES teachers(t_id) 
);

CREATE TABLE courses
(
	c_id CHAR(7)	NOT NULL,
	descr VARCHAR(40) NOT NULL,
	dept_id CHAR(4)		NOT NULL,
	credits INT		NOT NULL,
	
	PRIMARY KEY(c_id),
	FOREIGN KEY(dept_id) REFERENCES dept(dept_id)
);

CREATE TABLE enroll_info
(
	s_id INT		NOT NULL,
	c_id CHAR(7)	NOT NULL,
	status VARCHAR(2) NOT NULL,
	term VARCHAR(7) NOT NULL,
	grade INT		NOT NULL,

	PRIMARY KEY(s_id, c_id),
	FOREIGN KEY(c_id) REFERENCES courses(c_id),
	FOREIGN KEY(s_id) REFERENCES student(s_id)
);

CREATE TABLE sections
(
	c_id			char(7)			NOT NULL,
	sec_id			varchar(6)		NOT NULL,
	t_id			int				NOT NULL,
	sec_day			varchar(3)		NOT NULL,
	sec_time		char(11)		NOT NULL,
	term			varchar(7)		NOT NULL,
	PRIMARY KEY(c_id, sec_id),
	FOREIGN KEY(c_id) REFERENCES courses(c_id),
	
	CHECK (sec_time like '[0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9]')
);

CREATE TABLE student_courses
(
	s_id INT	NOT NULL,
	sec_id VARCHAR(6) NOT NULL,
	c_id   CHAR(7)    NOT NULL,
	
	PRIMARY KEY(s_id, sec_id),
	FOREIGN KEY(c_id) REFERENCES courses(c_id),
	FOREIGN KEY(c_id, sec_id) REFERENCES sections(c_id, sec_id),
	FOREIGN KEY(s_id) REFERENCES student(s_id)
);


CREATE TABLE requirements
(
	c_id			char(7)			NOT NULL,
	c_requirement	char(7)			NOT NULL,
	PRIMARY KEY(c_id, c_requirement),
	FOREIGN KEY(c_id) REFERENCES courses(c_id)
);

INSERT student VALUES(1, 'Michael', 'Sargent', '4 Morgan Dr', 'Bow', 'NH', '03304', 'M');
INSERT student VALUES(2, 'Jake', 'Chadwick', '123 Main St', 'Concord', 'NH', '03301', 'M');
INSERT student VALUES(3, 'Samantha', 'White', '145 Elm St', 'Manchester', 'NH', '03104', 'F');
INSERT student VALUES(4, 'Laura', 'Jobin', '87 Pine St', 'Hooksett', 'NH', '03106', 'F');

INSERT teachers VALUES(1, 'Kenneth', 'Polito', '603-123-4567', 80000.02);
INSERT teachers VALUES(2, 'Frank', 'Gitlitz', '603-987-6543', 80000.04);

INSERT dept VALUES('ENGL', 'English', 1);
INSERT dept VALUES('HIST', 'History', 1);
INSERT dept VALUES('MATH', 'Math', 2);
INSERT dept VALUES('COMP', 'Computer Science', 2);

INSERT courses VALUES('ENGL101', 'Intro To Spanish', 'ENGL', 4);
INSERT courses VALUES('COMP282', 'Turning On A Computer', 'COMP', 3);
INSERT courses VALUES('MATH123', '1 + 2 = Fish', 'MATH', 7);
INSERT courses VALUES('HIST969', 'Colonizing The Moon', 'HIST', 2);

INSERT sections VALUES('COMP282', 'Day', 1, 'MWF', '08:00-08:50', 'FAL2016');
INSERT sections VALUES('COMP282', 'Night', 1, 'TR', '17:00-18:20', 'FAL2016');
INSERT sections VALUES('MATH123', 'Online', 2, 'MWF', '12:00-12:50', 'SPR2017');
INSERT sections VALUES('ENGL101', 'Day', 1, 'TR', '02:00-03:20', 'SUM2017');
INSERT sections VALUES('HIST969', 'Online', 2, 'TR', '15:00-16:20', 'SUM2017');

INSERT enroll_info VALUES(1, 'COMP282', 'P', 'FAL2016', 97);
INSERT enroll_info VALUES(1, 'ENGL101', 'EN', 'SUM2017', 97);
INSERT enroll_info VALUES(1, 'MATH123', 'W', 'SPR2017', 74);
INSERT enroll_info VALUES(1, 'HIST969', 'EN', 'SUM2017', 86);
INSERT enroll_info VALUES(2, 'MATH123', 'P', 'SPR2017', 92);
INSERT enroll_info VALUES(2, 'HIST969', 'EN', 'SUM2017', 92);
INSERT enroll_info VALUES(3, 'ENGL101', 'EN', 'SUM2017', 91);
INSERT enroll_info VALUES(3, 'COMP282', 'F', 'FAL2016', 54);
INSERT enroll_info VALUES(4, 'COMP282', 'P', 'FAL2016', 91);
INSERT enroll_info VALUES(4, 'HIST969', 'EN', 'HIST969', 88);


