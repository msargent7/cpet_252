﻿namespace MidtermProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvStores = new System.Windows.Forms.ListView();
            this.lvBooks = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lvOrders = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.quitButton = new System.Windows.Forms.Button();
            this.addStoreButton = new System.Windows.Forms.Button();
            this.addBookButton = new System.Windows.Forms.Button();
            this.showOrdersButton = new System.Windows.Forms.Button();
            this.addOrderButton = new System.Windows.Forms.Button();
            this.updateOrdersButton = new System.Windows.Forms.Button();
            this.removeOrderButton = new System.Windows.Forms.Button();
            this.updateStoreButton = new System.Windows.Forms.Button();
            this.updateBookButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvStores
            // 
            this.lvStores.HideSelection = false;
            this.lvStores.Location = new System.Drawing.Point(12, 65);
            this.lvStores.MultiSelect = false;
            this.lvStores.Name = "lvStores";
            this.lvStores.Size = new System.Drawing.Size(548, 287);
            this.lvStores.TabIndex = 3;
            this.lvStores.UseCompatibleStateImageBehavior = false;
            this.lvStores.SelectedIndexChanged += new System.EventHandler(this.lvStores_SelectedIndexChanged);
            // 
            // lvBooks
            // 
            this.lvBooks.HideSelection = false;
            this.lvBooks.Location = new System.Drawing.Point(576, 65);
            this.lvBooks.MultiSelect = false;
            this.lvBooks.Name = "lvBooks";
            this.lvBooks.Size = new System.Drawing.Size(548, 287);
            this.lvBooks.TabIndex = 4;
            this.lvBooks.UseCompatibleStateImageBehavior = false;
            this.lvBooks.SelectedIndexChanged += new System.EventHandler(this.lvBooks_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(229, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 31);
            this.label1.TabIndex = 5;
            this.label1.Text = "Stores";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(796, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 31);
            this.label2.TabIndex = 6;
            this.label2.Text = "Books";
            // 
            // lvOrders
            // 
            this.lvOrders.Location = new System.Drawing.Point(152, 394);
            this.lvOrders.MultiSelect = false;
            this.lvOrders.Name = "lvOrders";
            this.lvOrders.Size = new System.Drawing.Size(821, 287);
            this.lvOrders.TabIndex = 7;
            this.lvOrders.UseCompatibleStateImageBehavior = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(486, 360);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 31);
            this.label3.TabIndex = 9;
            this.label3.Text = "Orders";
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(1043, 697);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(114, 38);
            this.quitButton.TabIndex = 10;
            this.quitButton.Text = "Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // addStoreButton
            // 
            this.addStoreButton.Location = new System.Drawing.Point(12, 394);
            this.addStoreButton.Name = "addStoreButton";
            this.addStoreButton.Size = new System.Drawing.Size(114, 38);
            this.addStoreButton.TabIndex = 11;
            this.addStoreButton.Text = "Add Store";
            this.addStoreButton.UseVisualStyleBackColor = true;
            this.addStoreButton.Click += new System.EventHandler(this.addStoreButton_Click);
            // 
            // addBookButton
            // 
            this.addBookButton.Location = new System.Drawing.Point(1010, 394);
            this.addBookButton.Name = "addBookButton";
            this.addBookButton.Size = new System.Drawing.Size(114, 38);
            this.addBookButton.TabIndex = 14;
            this.addBookButton.Text = "Add Book";
            this.addBookButton.UseVisualStyleBackColor = true;
            this.addBookButton.Click += new System.EventHandler(this.addBookButton_Click);
            // 
            // showOrdersButton
            // 
            this.showOrdersButton.Location = new System.Drawing.Point(249, 697);
            this.showOrdersButton.Name = "showOrdersButton";
            this.showOrdersButton.Size = new System.Drawing.Size(114, 38);
            this.showOrdersButton.TabIndex = 17;
            this.showOrdersButton.Text = "Show All Orders";
            this.showOrdersButton.UseVisualStyleBackColor = true;
            this.showOrdersButton.Click += new System.EventHandler(this.showOrdersButton_Click);
            // 
            // addOrderButton
            // 
            this.addOrderButton.Location = new System.Drawing.Point(410, 697);
            this.addOrderButton.Name = "addOrderButton";
            this.addOrderButton.Size = new System.Drawing.Size(114, 38);
            this.addOrderButton.TabIndex = 18;
            this.addOrderButton.Text = "Add Order";
            this.addOrderButton.UseVisualStyleBackColor = true;
            this.addOrderButton.Click += new System.EventHandler(this.addOrderButton_Click);
            // 
            // updateOrdersButton
            // 
            this.updateOrdersButton.Location = new System.Drawing.Point(568, 697);
            this.updateOrdersButton.Name = "updateOrdersButton";
            this.updateOrdersButton.Size = new System.Drawing.Size(114, 38);
            this.updateOrdersButton.TabIndex = 19;
            this.updateOrdersButton.Text = "Update Order";
            this.updateOrdersButton.UseVisualStyleBackColor = true;
            this.updateOrdersButton.Click += new System.EventHandler(this.updateOrdersButton_Click);
            // 
            // removeOrderButton
            // 
            this.removeOrderButton.Location = new System.Drawing.Point(714, 697);
            this.removeOrderButton.Name = "removeOrderButton";
            this.removeOrderButton.Size = new System.Drawing.Size(114, 38);
            this.removeOrderButton.TabIndex = 20;
            this.removeOrderButton.Text = "Remove Order";
            this.removeOrderButton.UseVisualStyleBackColor = true;
            this.removeOrderButton.Click += new System.EventHandler(this.removeOrderButton_Click);
            // 
            // updateStoreButton
            // 
            this.updateStoreButton.Location = new System.Drawing.Point(12, 452);
            this.updateStoreButton.Name = "updateStoreButton";
            this.updateStoreButton.Size = new System.Drawing.Size(114, 38);
            this.updateStoreButton.TabIndex = 12;
            this.updateStoreButton.Text = "Update Store";
            this.updateStoreButton.UseVisualStyleBackColor = true;
            this.updateStoreButton.Click += new System.EventHandler(this.updateStoreButton_Click);
            // 
            // updateBookButton
            // 
            this.updateBookButton.Location = new System.Drawing.Point(1010, 452);
            this.updateBookButton.Name = "updateBookButton";
            this.updateBookButton.Size = new System.Drawing.Size(114, 38);
            this.updateBookButton.TabIndex = 21;
            this.updateBookButton.Text = "Update Book";
            this.updateBookButton.UseVisualStyleBackColor = true;
            this.updateBookButton.Click += new System.EventHandler(this.updateBookButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 747);
            this.Controls.Add(this.updateBookButton);
            this.Controls.Add(this.removeOrderButton);
            this.Controls.Add(this.updateOrdersButton);
            this.Controls.Add(this.addOrderButton);
            this.Controls.Add(this.showOrdersButton);
            this.Controls.Add(this.addBookButton);
            this.Controls.Add(this.updateStoreButton);
            this.Controls.Add(this.addStoreButton);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lvOrders);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvBooks);
            this.Controls.Add(this.lvStores);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView lvStores;
        private System.Windows.Forms.ListView lvBooks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvOrders;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.Button addStoreButton;
        private System.Windows.Forms.Button addBookButton;
        private System.Windows.Forms.Button showOrdersButton;
        private System.Windows.Forms.Button addOrderButton;
        private System.Windows.Forms.Button updateOrdersButton;
        private System.Windows.Forms.Button removeOrderButton;
        private System.Windows.Forms.Button updateStoreButton;
        private System.Windows.Forms.Button updateBookButton;
    }
}

