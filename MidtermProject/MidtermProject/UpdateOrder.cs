﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidtermProject
{
    public partial class UpdateOrder : Form
    {
        Sales order;

        string serverIP = Form1.serverIP;
        public UpdateOrder(Sales s)
        {
            InitializeComponent();
            order = s;

            storeNameBox.Text = order.stor_id;
            orderNumBox.Text = order.ord_num;
            orderDateBox.Text = order.ord_date;
            qtyBox.Text = order.qty;
            payTermsComboBox.Text = order.payterms;
            titleIDBox.Text = order.title_id;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void updateOrderButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon!");

            /*string endPoint = "http://" + serverIP + "/sales/" + order.ord_num;

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "PUT";
            request.ContentLength = 0;
            request.ContentType = "text/json";*/
        }
    }
}
