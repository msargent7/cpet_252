﻿namespace MidtermProject
{
    partial class UpdateOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.updateOrderButton = new System.Windows.Forms.Button();
            this.titleIDBox = new System.Windows.Forms.TextBox();
            this.qtyBox = new System.Windows.Forms.TextBox();
            this.orderDateBox = new System.Windows.Forms.TextBox();
            this.orderNumBox = new System.Windows.Forms.TextBox();
            this.storeNameBox = new System.Windows.Forms.TextBox();
            this.payTermsComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(260, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Title:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(260, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Pay Terms:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(260, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Quantity:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Order Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Order Number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Store Name:";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(382, 215);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(113, 28);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // updateOrderButton
            // 
            this.updateOrderButton.Location = new System.Drawing.Point(263, 215);
            this.updateOrderButton.Name = "updateOrderButton";
            this.updateOrderButton.Size = new System.Drawing.Size(113, 28);
            this.updateOrderButton.TabIndex = 20;
            this.updateOrderButton.Text = "Update Order";
            this.updateOrderButton.UseVisualStyleBackColor = true;
            this.updateOrderButton.Click += new System.EventHandler(this.updateOrderButton_Click);
            // 
            // titleIDBox
            // 
            this.titleIDBox.Location = new System.Drawing.Point(263, 156);
            this.titleIDBox.Name = "titleIDBox";
            this.titleIDBox.ReadOnly = true;
            this.titleIDBox.Size = new System.Drawing.Size(145, 20);
            this.titleIDBox.TabIndex = 18;
            // 
            // qtyBox
            // 
            this.qtyBox.Location = new System.Drawing.Point(263, 45);
            this.qtyBox.Name = "qtyBox";
            this.qtyBox.Size = new System.Drawing.Size(145, 20);
            this.qtyBox.TabIndex = 17;
            // 
            // orderDateBox
            // 
            this.orderDateBox.Location = new System.Drawing.Point(12, 156);
            this.orderDateBox.Name = "orderDateBox";
            this.orderDateBox.ReadOnly = true;
            this.orderDateBox.Size = new System.Drawing.Size(145, 20);
            this.orderDateBox.TabIndex = 16;
            // 
            // orderNumBox
            // 
            this.orderNumBox.Location = new System.Drawing.Point(12, 99);
            this.orderNumBox.Name = "orderNumBox";
            this.orderNumBox.ReadOnly = true;
            this.orderNumBox.Size = new System.Drawing.Size(145, 20);
            this.orderNumBox.TabIndex = 15;
            // 
            // storeNameBox
            // 
            this.storeNameBox.Location = new System.Drawing.Point(12, 45);
            this.storeNameBox.Name = "storeNameBox";
            this.storeNameBox.ReadOnly = true;
            this.storeNameBox.Size = new System.Drawing.Size(145, 20);
            this.storeNameBox.TabIndex = 14;
            // 
            // payTermsComboBox
            // 
            this.payTermsComboBox.FormattingEnabled = true;
            this.payTermsComboBox.Items.AddRange(new object[] {
            "Net 60",
            "Net 30",
            "On Invoice"});
            this.payTermsComboBox.Location = new System.Drawing.Point(263, 99);
            this.payTermsComboBox.Name = "payTermsComboBox";
            this.payTermsComboBox.Size = new System.Drawing.Size(145, 21);
            this.payTermsComboBox.TabIndex = 28;
            // 
            // UpdateOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 271);
            this.Controls.Add(this.payTermsComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.updateOrderButton);
            this.Controls.Add(this.titleIDBox);
            this.Controls.Add(this.qtyBox);
            this.Controls.Add(this.orderDateBox);
            this.Controls.Add(this.orderNumBox);
            this.Controls.Add(this.storeNameBox);
            this.Name = "UpdateOrder";
            this.Text = "UpdateOrder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button updateOrderButton;
        private System.Windows.Forms.TextBox titleIDBox;
        private System.Windows.Forms.TextBox qtyBox;
        private System.Windows.Forms.TextBox orderDateBox;
        private System.Windows.Forms.TextBox orderNumBox;
        private System.Windows.Forms.TextBox storeNameBox;
        private System.Windows.Forms.ComboBox payTermsComboBox;
    }
}