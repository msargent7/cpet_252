﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidtermProject
{
    public partial class AddBook : Form
    {
        public AddBook()
        {
            InitializeComponent();

            titleIDBox.Text = titleIDGenerator();
        }

        public string titleIDGenerator()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[2];
            var random = new Random();
            string storeID;

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            Random slumpGenerator = new Random();
            string storeNum = slumpGenerator.Next(1000, 9999).ToString();

            storeID = stringChars[0].ToString() + stringChars[1].ToString() + storeNum;

            return storeID;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void createBookButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon!");
        }
    }

}
