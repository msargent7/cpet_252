﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// REST client includes 

//using System.Net;
//
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json; // be sure to add reference
using Newtonsoft.Json;
using System.Runtime.Serialization; // for [DataContract] attribute
using System.Net;

namespace MidtermProject
{
    public partial class Form1 : Form
    {
        public enum HttpVerb { GET, POST, PUT, DELETE }

        public static string serverIP = "64.72.2.54:8081";
        public Form1()
        {
            InitializeComponent();

            getStores();
            getBooks();
            getOrders();
        }

        public void loadListViewStores()
        {
            //Set up the visuals for the list view.
            lvStores.View = View.Details;
            lvStores.GridLines = true;
            lvStores.FullRowSelect = true;

            //Create columns
            lvStores.Columns.Add("ID");
            lvStores.Columns.Add("Name");
            lvStores.Columns.Add("Address");
            lvStores.Columns.Add("City");
            lvStores.Columns.Add("State");
            lvStores.Columns.Add("Zip Code");

            //Size columns
            lvStores.Columns[0].Width = -2;
            lvStores.Columns[1].Width = -2;
            lvStores.Columns[2].Width = -2;
            lvStores.Columns[3].Width = -2;
            lvStores.Columns[4].Width = -2;
            lvStores.Columns[5].Width = -2;
        }

        public void loadListViewBooks()
        {
            //Set up the visuals for the list view.
            lvBooks.View = View.Details;
            lvBooks.GridLines = true;
            lvBooks.FullRowSelect = true;

            //Create columns
            lvBooks.Columns.Add("ID");
            lvBooks.Columns.Add("Title");
            lvBooks.Columns.Add("Type");
            lvBooks.Columns.Add("Pub ID");
            lvBooks.Columns.Add("Price");
            lvBooks.Columns.Add("Advance");
            lvBooks.Columns.Add("Royalty");
            lvBooks.Columns.Add("Sales");
            lvBooks.Columns.Add("Notes");
            lvBooks.Columns.Add("Pub Date");

            //Size columns
            lvBooks.Columns[0].Width = -2;
            lvBooks.Columns[1].Width = -2;
            lvBooks.Columns[2].Width = -2;
            lvBooks.Columns[3].Width = -2;
            lvBooks.Columns[4].Width = -2;
            lvBooks.Columns[5].Width = -2;
            lvBooks.Columns[6].Width = -2;
            lvBooks.Columns[7].Width = -2;
            lvBooks.Columns[8].Width = -2;
            lvBooks.Columns[9].Width = -2;
        }

        public void loadListViewOrders()
        {
            //Set up the visuals for the list view.
            lvOrders.View = View.Details;
            lvOrders.GridLines = true;
            lvOrders.FullRowSelect = true;

            //Create columns
            lvOrders.Columns.Add("Store");
            lvOrders.Columns.Add("Order Number");
            lvOrders.Columns.Add("Order Date");
            lvOrders.Columns.Add("Quantity");
            lvOrders.Columns.Add("Pay Terms");
            lvOrders.Columns.Add("Title");

            //Size columns
            lvOrders.Columns[0].Width = -2;
            lvOrders.Columns[1].Width = -2;
            lvOrders.Columns[2].Width = -2;
            lvOrders.Columns[3].Width = -2;
            lvOrders.Columns[4].Width = -2;
            lvOrders.Columns[5].Width = -2;
        }

        public void getStores()
        {
            lvStores.Clear();
            string endPoint = "http://" + serverIP + "/stores";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/json";

            using (var response = (System.Net.HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;
                Store[] stores;


                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.StatusCode.ToString());
                }

                using (var responseStream = response.GetResponseStream())
                {

                    if (responseStream != null)
                        using (var reader = new System.IO.StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                            stores = JsonConvert.DeserializeObject<Store[]>(responseValue);
                            foreach (var s in stores)
                            {
                                var item = new ListViewItem(s.stor_id);
                                item.SubItems.Add(s.stor_name);
                                item.SubItems.Add(s.stor_address);
                                item.SubItems.Add(s.city);
                                item.SubItems.Add(s.state);
                                item.SubItems.Add(s.zip);

                                lvStores.Items.Add(item);
                            }
                        }
                }
                loadListViewStores();
            }
        }

        public void getBooks()
        {
            lvBooks.Clear();
            string endPoint = "http://" + serverIP + "/books";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/json";

            using (var response = (System.Net.HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;
                Book[] books;


                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.StatusCode.ToString());
                }

                using (var responseStream = response.GetResponseStream())
                {

                    if (responseStream != null)
                        using (var reader = new System.IO.StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                            books = JsonConvert.DeserializeObject<Book[]>(responseValue);
                            foreach (var b in books)
                            {
                                var item = new ListViewItem(b.title_id);
                                item.SubItems.Add(b.title);
                                item.SubItems.Add(b.type);
                                item.SubItems.Add(b.pub_id);
                                item.SubItems.Add(b.price);
                                item.SubItems.Add(b.advance);
                                item.SubItems.Add(b.royalty);
                                item.SubItems.Add(b.ytd_sales);
                                item.SubItems.Add(b.notes);
                                item.SubItems.Add(b.pubdate);

                                lvBooks.Items.Add(item);
                            }
                        }
                }
                loadListViewBooks();
            }
        }

        public void getOrders()
        {
            lvOrders.Clear();
            string endPoint = "http://" + serverIP + "/sales";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/json";

            using (var response = (System.Net.HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;
                Sales[] sales;

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new ApplicationException(response.StatusCode.ToString());
                }

                using (var responseStream = response.GetResponseStream())
                {

                    if (responseStream != null)
                        using (var reader = new System.IO.StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                            sales = JsonConvert.DeserializeObject<Sales[]>(responseValue);
                            foreach (var s in sales)
                            {
                                var item = new ListViewItem(s.stor_name);
                                item.SubItems.Add(s.ord_num);
                                item.SubItems.Add(s.ord_date);
                                item.SubItems.Add(s.qty);
                                item.SubItems.Add(s.payterms);
                                item.SubItems.Add(s.title);

                                lvOrders.Items.Add(item);
                            }
                        }
                }
                loadListViewOrders();
            }
        }

        private void lvStores_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvOrders.Clear();

            if (lvStores.SelectedItems.Count > 0)
            {
                string storeId = lvStores.SelectedItems[0].Text;

                if (storeId == null)
                {
                    MessageBox.Show("Please select a store");
                    return;
                }

                else
                {
                    string endPoint = "http://" + serverIP + "/sales/store/" + storeId;

                    var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

                    request.Method = "GET";
                    request.ContentLength = 0;
                    request.ContentType = "text/json";

                    using (var response = (System.Net.HttpWebResponse)request.GetResponse())
                    {
                        var responseValue = string.Empty;
                        Sales[] sales;

                        if (response.StatusCode != System.Net.HttpStatusCode.OK)
                        {
                            throw new ApplicationException(response.StatusCode.ToString());
                        }

                        using (var responseStream = response.GetResponseStream())
                        {

                            if (responseStream != null)
                                using (var reader = new System.IO.StreamReader(responseStream))
                                {
                                    responseValue = reader.ReadToEnd();
                                    sales = JsonConvert.DeserializeObject<Sales[]>(responseValue);
                                    foreach (var s in sales)
                                    {
                                        var item = new ListViewItem(s.stor_name);
                                        item.SubItems.Add(s.ord_num);
                                        item.SubItems.Add(s.ord_date);
                                        item.SubItems.Add(s.qty);
                                        item.SubItems.Add(s.payterms);
                                        item.SubItems.Add(s.title);

                                        lvOrders.Items.Add(item);
                                    }
                                }
                        }
                        loadListViewOrders();
                    }
                }
            }

            else
                return;
        }

        private void lvBooks_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvOrders.Clear();

            if (lvBooks.SelectedItems.Count > 0)
            {
                string titleId =lvBooks.SelectedItems[0].Text;
                string endPoint = "http://" + serverIP + "/sales/book/" + titleId;

                var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(endPoint);

                request.Method = "GET";
                request.ContentLength = 0;
                request.ContentType = "text/json";

                using (var response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;
                    Sales[] sales;

                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        throw new ApplicationException(response.StatusCode.ToString());
                    }

                    using (var responseStream = response.GetResponseStream())
                    {

                        if (responseStream != null)
                            using (var reader = new System.IO.StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                                sales = JsonConvert.DeserializeObject<Sales[]>(responseValue);
                                foreach (var s in sales)
                                {
                                    var item = new ListViewItem(s.stor_name);
                                    item.SubItems.Add(s.ord_num);
                                    item.SubItems.Add(s.ord_date);
                                    item.SubItems.Add(s.qty);
                                    item.SubItems.Add(s.payterms);
                                    item.SubItems.Add(s.title);

                                    lvOrders.Items.Add(item);
                                }
                            }
                    }
                    loadListViewOrders();
                }
            }

            else
                return;
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void showOrdersButton_Click(object sender, EventArgs e)
        {
            getOrders();
        }

        private void removeStoreButton_Click(object sender, EventArgs e)
        {

            if (lvStores.SelectedItems.Count > 0)
            {
                string storeId = lvStores.SelectedItems[0].Text;

                if (storeId == null)
                {
                    MessageBox.Show("Please select a store");
                    return;
                }

                else
                {
                    string endPoint = "http://" + serverIP + "/stores/" + storeId;

                    WebRequest request = WebRequest.Create(endPoint);
                    request.Method = "DELETE";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }

                getStores();
            }
        }

        private void removeOrderButton_Click(object sender, EventArgs e)
        {
            if (lvOrders.SelectedItems.Count > 0)
            {
                string orderNum = lvOrders.SelectedItems[0].SubItems[1].Text;

                if (orderNum == null)
                {
                    MessageBox.Show("Please select an order");
                    return;
                }

                else
                {
                    string endPoint = "http://" + serverIP + "/sales/" + orderNum;

                    WebRequest request = WebRequest.Create(endPoint);
                    request.Method = "DELETE";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }

                getOrders();
            }

            else
            {
                MessageBox.Show("Please select an order");
            }
        }

        private void addOrderButton_Click(object sender, EventArgs e)
        {
            if ((lvStores.SelectedItems.Count > 0) && (lvBooks.SelectedItems.Count > 0))
            {
                Store store = new Store();
                store.stor_id = lvStores.SelectedItems[0].Text;
                store.stor_name = lvStores.SelectedItems[0].SubItems[1].Text;
                store.stor_address = lvStores.SelectedItems[0].SubItems[2].Text;
                store.city = lvStores.SelectedItems[0].SubItems[3].Text;
                store.state = lvStores.SelectedItems[0].SubItems[4].Text;
                store.zip = lvStores.SelectedItems[0].SubItems[5].Text;

                Book book = new Book();
                book.title_id = lvBooks.SelectedItems[0].Text;
                book.title = lvBooks.SelectedItems[0].SubItems[1].Text;
                book.type = lvBooks.SelectedItems[0].SubItems[2].Text;
                book.pub_id = lvBooks.SelectedItems[0].SubItems[3].Text;
                book.price = lvBooks.SelectedItems[0].SubItems[4].Text;
                book.advance = lvBooks.SelectedItems[0].SubItems[5].Text;
                book.royalty = lvBooks.SelectedItems[0].SubItems[6].Text;
                book.ytd_sales = lvBooks.SelectedItems[0].SubItems[7].Text;
                book.notes = lvBooks.SelectedItems[0].SubItems[8].Text;
                book.pubdate = lvBooks.SelectedItems[0].SubItems[9].Text;

                AddOrder newWindow = new AddOrder(store, book);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select a store and a book");
                return;
            }
        }

        private void updateOrdersButton_Click(object sender, EventArgs e)
        {
            if (lvOrders.SelectedItems.Count > 0)
            {
                Sales order = new Sales();
                order.stor_id = lvOrders.SelectedItems[0].Text;
                order.ord_num = lvOrders.SelectedItems[0].SubItems[1].Text;
                order.ord_date = lvOrders.SelectedItems[0].SubItems[2].Text;
                order.qty = lvOrders.SelectedItems[0].SubItems[3].Text;
                order.payterms = lvOrders.SelectedItems[0].SubItems[4].Text;
                order.title_id = lvOrders.SelectedItems[0].SubItems[5].Text;

                UpdateOrder newWindow = new UpdateOrder(order);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select an order");
                return;
            }
        }

        private void addStoreButton_Click(object sender, EventArgs e)
        {
            AddStore newWindow = new AddStore();
            newWindow.ShowDialog();
        }

        private void updateStoreButton_Click(object sender, EventArgs e)
        {
            if (lvStores.SelectedItems.Count > 0)
            {
                Store store = new Store();
                store.stor_id = lvStores.SelectedItems[0].Text;
                store.stor_name = lvStores.SelectedItems[0].SubItems[1].Text;
                store.stor_address = lvStores.SelectedItems[0].SubItems[2].Text;
                store.city = lvStores.SelectedItems[0].SubItems[3].Text;
                store.state = lvStores.SelectedItems[0].SubItems[4].Text;
                store.zip = lvStores.SelectedItems[0].SubItems[5].Text;

                UpdateStore newWindow = new UpdateStore(store);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select an store");
                return;
            }
        }

        private void addBookButton_Click(object sender, EventArgs e)
        {
            AddBook newWindow = new AddBook();
            newWindow.ShowDialog();
        }

        private void updateBookButton_Click(object sender, EventArgs e)
        {
            if (lvBooks.SelectedItems.Count > 0)
            {
                Book book = new Book();
                book.title_id = lvBooks.SelectedItems[0].Text;
                book.title = lvBooks.SelectedItems[0].SubItems[1].Text;
                book.type = lvBooks.SelectedItems[0].SubItems[2].Text;
                book.pub_id = lvBooks.SelectedItems[0].SubItems[3].Text;
                book.price = lvBooks.SelectedItems[0].SubItems[4].Text;
                book.advance = lvBooks.SelectedItems[0].SubItems[5].Text;
                book.royalty = lvBooks.SelectedItems[0].SubItems[6].Text;
                book.ytd_sales = lvBooks.SelectedItems[0].SubItems[7].Text;
                book.notes = lvBooks.SelectedItems[0].SubItems[8].Text;
                book.pubdate = lvBooks.SelectedItems[0].SubItems[9].Text;

                UpdateBook newWindow = new UpdateBook(book);
                newWindow.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please select a book");
                return;
            }
        }
    }

    [DataContract]
    public class Store
    {
        [DataMember]
        public string stor_id { get; set; }
        [DataMember]
        public string stor_name { get; set; }
        [DataMember]
        public string stor_address { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string zip { get; set; }
    }

    [DataContract]
    public class Sales
    {
        [DataMember]
        public string stor_name { get; set; }
        [DataMember]
        public string stor_id { get; set; }
        [DataMember]
        public string ord_num { get; set; }
        [DataMember]
        public string ord_date { get; set; }
        [DataMember]
        public string qty { get; set; }
        [DataMember]
        public string payterms { get; set; }
        [DataMember]
        public string title_id { get; set; }
        [DataMember]
        public string title { get; set; }
    }

    [DataContract]
    public class Author
    {
        [DataMember]
        public string au_id { get; set; }
        [DataMember]
        public string au_lname { get; set; }
        [DataMember]
        public string au_fname { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string zip { get; set; }
        [DataMember]
        public string contract { get; set; }
    }

    [DataContract]
    public class Book
    {
        [DataMember]
        public string title_id { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string pub_id { get; set; }
        [DataMember]
        public string price { get; set; }
        [DataMember]
        public string advance { get; set; }
        [DataMember]
        public string royalty { get; set; }
        [DataMember]
        public string ytd_sales { get; set; }
        [DataMember]
        public string notes { get; set; }
        [DataMember]
        public string pubdate { get; set; }
    }
}
