﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidtermProject
{
    public partial class AddStore : Form
    {
        string serverIP = Form1.serverIP;
        public AddStore()
        {
            InitializeComponent();

            storeIDBox.Text = generateStoreID().ToString();
        }

        public int generateStoreID()
        {
            Random slumpGenerator = new Random();
            int storeID = slumpGenerator.Next(1000, 9999);

            return storeID;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void createStoreButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon!");
        }
    }
}
