﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// REST client includes 

//using System.Net;
//
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json; // be sure to add reference
using Newtonsoft.Json;
using System.Runtime.Serialization; // for [DataContract] attribute
using System.Net;

namespace MidtermProject
{
    public partial class AddOrder : Form
    {
        string serverIP = Form1.serverIP;
        public AddOrder(Store s, Book b)
        {
            InitializeComponent();

            storeIDBox.Text = s.stor_name;
            titleIDBox.Text = b.title;
            orderNumBox.Text = generateOrderNum().ToString();
            orderDateBox.Text = DateTime.Now.ToShortDateString();
        }

        public int generateOrderNum()
        {
            Random slumpGenerator = new Random();
            int orderNum = slumpGenerator.Next(1000, 99999999);

            return orderNum;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void createOrderButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon!");
        }
    }
}
